package id.ui.cs.overtale_battle.service;

import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import id.ui.cs.overtale_battle.exceptions.*;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.repository.BattlePlayerRepository;
import id.ui.cs.overtale_battle.repository.PartyRepository;
import id.ui.cs.overtale_battle.repository.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PartyServiceTest {
    @Mock
    PartyRepository partyRepository;

    @Mock
    BattlePlayerRepository battlePlayerRepository;

    @InjectMocks
    PartyServiceImpl partyService;

    Player player;

    BattlePlayer battlePlayer;

    Party party;

    @BeforeEach
    void init() {
        player = new Player("Coeg", 0, 0, 0, 0, 0, 0, 0, "Knight", new StatsModifier(), new HashMap<>());
        battlePlayer = new BattlePlayer(player);
        party = new Party("Coeg", battlePlayer);
    }

    @Test
    void getParties() {
        when(partyRepository.findAll()).thenReturn(new ArrayList<>());
        assertTrue(partyService.getParties().isEmpty());
    }

    @Test
    void getParty() {
        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(party);
        assertEquals(party, partyService.getParty(party.getPartyName()));
    }

    @Test
    void createParty() throws Exception {
        when(battlePlayerRepository.save(any(Player.class))).thenReturn(battlePlayer);

        battlePlayer.setParty(party);
        assertThrows(PlayerAlreadyInPartyException.class, () -> {
            partyService.createParty(party.getPartyName(), player);
        });

        battlePlayer.setParty(null);
        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(party);
        assertThrows(PartyNameAlreadyTakenException.class, () -> {
            partyService.createParty(party.getPartyName(), player);
        });

        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(null);
        assertEquals(party.getPartyName(), partyService.createParty(party.getPartyName(), player).getPartyName());
    }

    @Test
    void addPlayer() throws Exception {
        when(battlePlayerRepository.save(any(Player.class))).thenReturn(battlePlayer);

        battlePlayer.setParty(party);
        assertThrows(PlayerAlreadyInPartyException.class, () -> {
            partyService.addPlayer(party.getPartyName(), player);
        });

        battlePlayer.setParty(null);
        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(null);
        assertThrows(PartyNotFoundException.class, () -> {
            partyService.addPlayer(party.getPartyName(), player);
        });

        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(party);
        party.setStatus(PartyState.FINISHED);
        assertThrows(PartyAlreadyStartedException.class, () -> {
            partyService.addPlayer(party.getPartyName(), player);
        });

        party.setStatus(PartyState.OPEN);
        party.setPlayers(List.of(new BattlePlayer(player), new BattlePlayer(player), new BattlePlayer(player), new BattlePlayer(player)));
        assertThrows(PartyAlreadyFullException.class, () -> {
            partyService.addPlayer(party.getPartyName(), player);
        });

        party.setPlayers(new ArrayList<>());
        party.getPlayers().add(battlePlayer);
        assertEquals(party.getPartyName(), partyService.addPlayer(party.getPartyName(), player).getPartyName());
    }

    @Test
    void disbandParty() throws Exception {
        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(null);
        assertThrows(PartyNotFoundException.class, () -> {
            partyService.disbandParty(party.getPartyName());
        });

        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(party);
        assertTrue(partyService.disbandParty(party.getPartyName()) );
    }

    @Test
    void disbandPlayer() throws Exception {
        when(battlePlayerRepository.findByName(player.getName())).thenReturn(null);
        assertThrows(PlayerNotFoundException.class, () -> {
            partyService.disbandPlayer(party.getPartyName(), player.getName());
        });

        when(battlePlayerRepository.findByName(player.getName())).thenReturn(battlePlayer);
        battlePlayer.setParty(null);
        assertThrows(PlayerNotInPartyException.class, () -> {
            partyService.disbandPlayer(party.getPartyName(), player.getName());
        });

        battlePlayer.setParty(party);
        party.setPartyLeader(battlePlayer);
        assertThrows(PlayerIsPartyLeaderException.class, () -> {
            partyService.disbandPlayer(party.getPartyName(), player.getName());
        });

        party.setPartyLeader(new BattlePlayer(player));
        assertTrue(partyService.disbandPlayer(party.getPartyName(), player.getName()));
    }

    @Test
    void getPlayerReturnPlayer() {
        when(battlePlayerRepository.findByName(player.getName())).thenReturn(battlePlayer);
        assertEquals(battlePlayer, partyService.getPlayer(player.getName()));
    }
}
