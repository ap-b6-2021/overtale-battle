package id.ui.cs.overtale_battle.service;

import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.entities.Monster;
import id.ui.cs.overtale_battle.core.entities.monsters.KingOgre;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import id.ui.cs.overtale_battle.exceptions.*;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.repository.BattlePlayerRepository;
import id.ui.cs.overtale_battle.repository.PartyRepository;
import id.ui.cs.overtale_battle.repository.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BattleServiceTest {

    @Mock
    PartyRepository partyRepository;

    @Mock
    PlayerRepository playerRepository;

    @Mock
    BattlePlayerRepository battlePlayerRepository;

    @InjectMocks
    BattleServiceImpl battleService;

    Player player;

    BattlePlayer battlePlayer;

    Monster monster;

    Party party;

    @BeforeEach
    public void init() {
        player = new Player("Coeg", 0, 0, 0, 0, 0, 0, 0, "Knight", new StatsModifier(), new HashMap<>());
        battlePlayer = new BattlePlayer(player);
        monster = new Monster(new KingOgre(), "King Ogre");
        party = new Party("Coeg", battlePlayer);
        HashMap<Integer, Monster> monsters = new HashMap<>();
        monsters.put(0, monster);
        party.setMonsters(monsters);
    }

    @Test
    void startBattle() throws Exception {
        assertThrows(PartyNotFoundException.class, () -> {
            battleService.startBattle(party.getPartyName(), List.of(player));
        });

        when(partyRepository.findByPartyName(party.getPartyName())).thenReturn(party);
        party.setStatus(PartyState.STARTED);
        assertThrows(PartyNotOpenException.class, () -> {
            battleService.startBattle(party.getPartyName(), List.of(player));
        });

        party.setStatus(PartyState.OPEN);
        assertEquals(party, battleService.startBattle(party.getPartyName(), List.of(player)));
    }

    @Test
    void buff() throws Exception {
        assertThrows(PlayerNotFoundException.class, () -> {
            battleService.buff(player.getName(), new StatsModifier());
        });

        when(battlePlayerRepository.findByName(player.getName())).thenReturn(battlePlayer);
        battlePlayer.setParty(null);
        assertThrows(PlayerNotInPartyException.class, () -> {
            battleService.buff(player.getName(), new StatsModifier());
        });

        battlePlayer.setParty(party);
        party.setStatus(PartyState.FINISHED);
        assertThrows(PartyNotStartedException.class, () -> {
            battleService.buff(player.getName(), new StatsModifier());
        });

        party.setStatus(PartyState.STARTED);
        party.setTurn("nyeh");
        assertThrows(PlayerIsNotInTurnException.class, () -> {
            battleService.buff(player.getName(), new StatsModifier());
        });

        party.setTurn(player.getName());
        party.setTurnI(0);
        party.setPlayers(List.of(battlePlayer));
        battlePlayer.setHealth(100);
        assertNotNull(battleService.buff(player.getName(), new StatsModifier()));

        monster.setHealth(0);
        assertNotNull(battleService.buff(player.getName(), new StatsModifier()));
    }

    @Test
    void attack() throws Exception {
        assertThrows(PlayerNotFoundException.class, () -> {
            battleService.attack(player.getName(), 0);
        });

        when(battlePlayerRepository.findByName(player.getName())).thenReturn(battlePlayer);
        battlePlayer.setParty(null);
        assertThrows(PlayerNotInPartyException.class, () -> {
            battleService.attack(player.getName(), 0);
        });

        battlePlayer.setParty(party);
        party.setStatus(PartyState.FINISHED);
        assertThrows(PartyNotStartedException.class, () -> {
            battleService.attack(player.getName(), 0);
        });

        party.setStatus(PartyState.STARTED);
        assertThrows(MonsterDoesNotExistsException.class, () -> {
            battleService.attack(player.getName(), 1);
        });

        party.setTurn("nyeh");
        assertThrows(PlayerIsNotInTurnException.class, () -> {
            battleService.attack(player.getName(), 0);
        });

        party.setTurn(player.getName());
        battlePlayer.setState(EntityState.DEAD);
        assertThrows(PlayerNotAliveException.class, () -> {
            battleService.attack(player.getName(), 0);
        });

        battlePlayer.setState(EntityState.ALIVE);
        monster.setState(EntityState.DEAD);
        assertThrows(MonsterNotAliveException.class, () -> {
            battleService.attack(player.getName(), 0);
        });

        monster.setState(EntityState.ALIVE);
        party.setTurnI(0);
        battlePlayer.setHealth(100);
        assertNotNull(battleService.attack(player.getName(), 0));

        monster.setHealth(0);
        assertNotNull(battleService.buff(player.getName(), new StatsModifier()));
    }
}
