package id.ui.cs.overtale_battle.controller;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.entities.Monster;
import id.ui.cs.overtale_battle.core.entities.monsters.KingOgre;
import id.ui.cs.overtale_battle.exceptions.PartyNotFoundException;
import id.ui.cs.overtale_battle.exceptions.PlayerAlreadyInPartyException;
import id.ui.cs.overtale_battle.exceptions.PlayerIsPartyLeaderException;
import id.ui.cs.overtale_battle.exceptions.PlayerNotInPartyException;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.service.PartyService;
import id.ui.cs.overtale_battle.service.PartyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;

@WebMvcTest(controllers = PartyController.class)
public class PartyControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PartyServiceImpl partyService;

    Player player;

    BattlePlayer battlePlayer;

    Party party;

    @BeforeEach
    public void init() {
        player = new Player("Coeg", 0, 0, 0, 0, 0, 0, 0, "Knight", new StatsModifier(), new HashMap<>());
        battlePlayer = new BattlePlayer(player);
        party = new Party("Coeg", battlePlayer);
        battlePlayer.setParty(party);
        HashMap<Integer, Monster> monsters = new HashMap<>();
        party.setMonsters(monsters);
    }

    @Test
    void testGetParties() throws Exception {
        when(partyService.getParties()).thenReturn(new ArrayList<>());
        mvc.perform(get("/party/get")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath(String.format("$")).isArray());
    }

    @Test
    void testGetParty() throws Exception {
        when(partyService.getParty(party.getPartyName())).thenReturn(party);
        mvc.perform(get(String.format("/party/get/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testGetPartyFail() throws Exception {
        mvc.perform(get(String.format("/party/get/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Party with name %s not found", party.getPartyName())));
    }

    @Test
    void testGetPartyFromPlayer() throws Exception {
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        mvc.perform(get(String.format("/party/get/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testGetPartyFromPlayerNotFound() throws Exception {
        mvc.perform(get(String.format("/party/get/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Player with name %s not found!", player.getName())));
    }

    @Test
    void testGetPartyFromPlayerPartyNotFound() throws Exception {
        battlePlayer.setParty(null);
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        mvc.perform(get(String.format("/party/get/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Player with name %s isn't in a party!", player.getName())));
    }

    @Test
    void testGetPlayerInfo() throws Exception {
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        mvc.perform(get(String.format("/party/get/playerInfo/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.player.name").value(battlePlayer.getPlayer().getName()));
    }

    @Test
    void testGetPlayerInfoFail() throws Exception {
        mvc.perform(get(String.format("/party/get/playerInfo/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string(String.format("Player with name %s not found!", player.getName())));
    }

    @Test
    void testCreateParty() throws Exception {
        when(partyService.createParty(party.getPartyName(), player)).thenReturn(party);
        mvc.perform(post(String.format("/party/create/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(player)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testCreatePartyNull() throws Exception {
        when(partyService.createParty(party.getPartyName(), player)).thenReturn(null);
        mvc.perform(post(String.format("/party/create/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(player)))
                .andExpect(status().isConflict());
    }

    @Test
    void testCreatePartyFail() throws Exception {
        when(partyService.createParty(party.getPartyName(), player)).thenThrow(new PlayerAlreadyInPartyException("Coeg"));
        mvc.perform(post(String.format("/party/create/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(player)))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    @Test
    void testAddPlayer() throws Exception {
        when(partyService.addPlayer(anyString(), any(Player.class))).thenReturn(party);
        mvc.perform(put(String.format("/party/add/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(player)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testAddPlayerNull() throws Exception {
        mvc.perform(put(String.format("/party/add/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(player)))
                .andExpect(status().isConflict());
    }

    @Test
    void testAddPlayerFail() throws Exception {
        when(partyService.addPlayer(anyString(), any(Player.class))).thenThrow(new PartyNotFoundException("Coeg"));
        mvc.perform(put(String.format("/party/add/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(asJsonString(player)))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    @Test
    void testDisbandParty() throws Exception {
        when(partyService.disbandParty(party.getPartyName())).thenReturn(true);
        mvc.perform(delete(String.format("/party/disband/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void testDisbandPartyNotFound() throws Exception {
        when(partyService.disbandParty(party.getPartyName())).thenReturn(false);
        mvc.perform(delete(String.format("/party/disband/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void testDisbandPartyFail() throws Exception {
        when(partyService.disbandParty(party.getPartyName())).thenThrow(new PartyNotFoundException("Coeg"));
        mvc.perform(delete(String.format("/party/disband/%s", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    @Test
    void testDisbandPlayer() throws Exception {
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        when(partyService.disbandPlayer(party.getPartyName(), player.getName())).thenReturn(true);
        mvc.perform(delete(String.format("/party/disband/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testDisbandPlayerNoParty() throws Exception {
        battlePlayer.setParty(null);
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        mvc.perform(delete(String.format("/party/disband/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void testDisbandPlayerPartyNotFound() throws Exception {
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        when(partyService.disbandPlayer(party.getPartyName(), player.getName())).thenReturn(false);
        mvc.perform(delete(String.format("/party/disband/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void testDisbandPlayerFail() throws Exception {
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        when(partyService.disbandPlayer(party.getPartyName(), player.getName())).thenThrow(new PlayerNotInPartyException("Coeg"));
        mvc.perform(delete(String.format("/party/disband/player/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    @Test
    void testDisbandPlayerFromParty() throws Exception {
        when(partyService.disbandPlayer(party.getPartyName(), player.getName())).thenReturn(true);
        when(partyService.getParty(party.getPartyName())).thenReturn(party);
        mvc.perform(delete(String.format("/party/disband/%s/%s", party.getPartyName(), player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testDisbandPlayerFromPartyNotFound() throws Exception {
        when(partyService.disbandPlayer(party.getPartyName(), player.getName())).thenReturn(false);
        mvc.perform(delete(String.format("/party/disband/%s/%s", party.getPartyName(), player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void testDisbandPlayerFromPartyFail() throws Exception {
        when(partyService.disbandPlayer(party.getPartyName(), player.getName())).thenThrow(new PlayerIsPartyLeaderException("Coeg"));
        mvc.perform(delete(String.format("/party/disband/%s/%s", party.getPartyName(), player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            return ow.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
