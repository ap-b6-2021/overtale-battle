package id.ui.cs.overtale_battle.controller;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.battle.Reward;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.entities.Entity;
import id.ui.cs.overtale_battle.core.entities.Monster;
import id.ui.cs.overtale_battle.core.entities.monsters.KingOgre;
import id.ui.cs.overtale_battle.exceptions.PartyNotFoundException;
import id.ui.cs.overtale_battle.exceptions.PlayerNotFoundException;
import id.ui.cs.overtale_battle.model.DTOs.AttackDto;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.service.BattleServiceImpl;
import id.ui.cs.overtale_battle.service.PartyServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;

@WebMvcTest(controllers = BattleController.class)
public class BattleControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PartyServiceImpl partyService;

    @MockBean
    private BattleServiceImpl battleService;

    Player player;

    BattlePlayer battlePlayer;

    Monster monster;

    Party party;

    @BeforeEach
    public void init() {
        player = new Player("Coeg", 0, 0, 0, 0, 0, 0, 0, "Knight", new StatsModifier(), new HashMap<>());
        battlePlayer = new BattlePlayer(player);
        monster = new Monster(new KingOgre(), "King Ogre");
        party = new Party("Coeg", battlePlayer);
        HashMap<Integer, Monster> monsters = new HashMap<>();
        monsters.put(0, monster);
        party.setMonsters(monsters);
    }

    @Test
    void testGetMonsters() throws Exception {
        when(partyService.getParty(party.getPartyName())).thenReturn(party);
        mvc.perform(get(String.format("/battle/get/%s/monsters", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(String.format("$.%d.id", 0)).value(monster.getId()));
    }

    @Test
    void testGetMonstersPartyNotFound() throws Exception {
        mvc.perform(get(String.format("/battle/get/%s/monsters", party.getPartyName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetReward() throws Exception {
        Reward reward = new Reward(100);
        battlePlayer.setReward(reward);
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        mvc.perform(get(String.format("/battle/get/reward/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.exp").value(reward.getExp()));
    }

    @Test
    void testGetRewardPlayerNotFound() throws Exception {
        mvc.perform(get(String.format("/battle/get/reward/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Player not found!"));
    }

    @Test
    void testGetRewardRewardNotFound() throws Exception {
        when(partyService.getPlayer(player.getName())).thenReturn(battlePlayer);
        mvc.perform(get(String.format("/battle/get/reward/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Player has no reward!"));
    }

    @Test
    void testStartBattle() throws Exception {
        when(battleService.startBattle(anyString(), anyList())).thenReturn(party);
        mvc.perform(post(String.format("/battle/start/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(Lists.list(player))))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.partyName").value(party.getPartyName()));
    }

    @Test
    void testStartBattleFail() throws Exception {
        when(battleService.startBattle(anyString(), anyList())).thenThrow(new PartyNotFoundException("Huha"));
        mvc.perform(post(String.format("/battle/start/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(Lists.list(player))))
                .andExpect(status().isConflict())
                .andExpect(content().string("Huha"));
    }

    @Test
    void testAttack() throws Exception {
        HashMap<String, AttackDto> attack = new HashMap<>();
        attack.put("player", new AttackDto(null, null, null, null));
        when(battleService.attack(anyString(), anyInt())).thenReturn(attack);
        mvc.perform(post(String.format("/battle/attack/%s/%d", player.getName(), 0))
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(Lists.list(player))))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.player").isMap());
    }

    @Test
    void testAttackFail() throws Exception {
        when(battleService.attack(anyString(), anyInt())).thenThrow(new PlayerNotFoundException("Coeg"));
        mvc.perform(post(String.format("/battle/attack/%s/%d", player.getName(), 0))
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(Lists.list(player))))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    @Test
    void testBuff() throws Exception {
        HashMap<String, AttackDto> attack = new HashMap<>();
        attack.put("player", new AttackDto(null, null, null, null));
        StatsModifier modifier = new StatsModifier();
        when(battleService.buff(anyString(), any(StatsModifier.class))).thenReturn(attack);
        mvc.perform(post(String.format("/battle/buff/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(attack)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.player").isMap());
    }

    @Test
    void testBuffFail() throws Exception {
        HashMap<String, AttackDto> attack = new HashMap<>();
        attack.put("player", new AttackDto(null, null, null, null));
        when(battleService.buff(anyString(), any(StatsModifier.class))).thenThrow(new PlayerNotFoundException("Coeg"));
        mvc.perform(post(String.format("/battle/buff/%s", player.getName()))
                .contentType(MediaType.APPLICATION_JSON).content(asJsonString(attack)))
                .andExpect(status().isConflict())
                .andExpect(content().string("Coeg"));
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            final String jsonContent = ow.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
