package id.ui.cs.overtale_battle.core.entities.monsters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class KingOgreTest {
    private Class<?> kingOgreClass;

    @BeforeEach
    public void setUp() throws Exception {
        kingOgreClass = Class.forName("id.ui.cs.overtale_battle.core.entities.monsters.KingOgre");
    }

    @Test
    public void testKingOgreIsConcreteCLass() {
        assertFalse(Modifier.isAbstract(kingOgreClass.getModifiers()));
    }

    @Test
    public void testKingOgreIsAMonsterAttacks() {
        Class<?> parentClass = kingOgreClass.getSuperclass();
        assertEquals("id.ui.cs.overtale_battle.core.entities.MonsterAttacks", parentClass.getName());
    }

    @Test
    public void testOverrideAttackDamage() throws Exception {
        Method attackDamage = kingOgreClass.getDeclaredMethod("attackDamage");
        assertEquals("java.lang.Integer", attackDamage.getGenericReturnType().getTypeName());
        assertEquals(0, attackDamage.getParameterCount());
    }

    @Test
    public void testOverrideDefense() throws Exception {
        Method defense = kingOgreClass.getDeclaredMethod("defense");
        assertEquals("java.lang.Integer", defense.getGenericReturnType().getTypeName());
        assertEquals(0, defense.getParameterCount());
    }

    @Test
    public void testOverrideAgility() throws Exception {
        Method agility = kingOgreClass.getDeclaredMethod("agility");
        assertEquals("java.lang.Integer", agility.getGenericReturnType().getTypeName());
        assertEquals(0, agility.getParameterCount());
    }
}
