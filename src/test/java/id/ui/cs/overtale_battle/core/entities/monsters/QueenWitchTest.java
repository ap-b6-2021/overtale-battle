package id.ui.cs.overtale_battle.core.entities.monsters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class QueenWitchTest {
    private Class<?> queenWitchClass;
    private QueenWitch queenWitch;

    @BeforeEach
    public void setUp() throws Exception {
        queenWitchClass = Class.forName("id.ui.cs.overtale_battle.core.entities.monsters.QueenWitch");
    }

    @Test
    public void testQueenWitchIsConcreteCLass() {
        assertFalse(Modifier.isAbstract(queenWitchClass.getModifiers()));
    }

    @Test
    public void testQueenWitchIsAMonsterAttacks() {
        Class<?> parentClass = queenWitchClass.getSuperclass();
        assertEquals("id.ui.cs.overtale_battle.core.entities.MonsterAttacks", parentClass.getName());
    }

    @Test
    public void testOverrideAttackDamage() throws Exception {
        Method attackDamage = queenWitchClass.getDeclaredMethod("attackDamage");
        assertEquals("java.lang.Integer", attackDamage.getGenericReturnType().getTypeName());
        assertEquals(0, attackDamage.getParameterCount());
    }

    @Test
    public void testOverrideDefense() throws Exception {
        Method defense = queenWitchClass.getDeclaredMethod("defense");
        assertEquals("java.lang.Integer", defense.getGenericReturnType().getTypeName());
        assertEquals(0, defense.getParameterCount());
    }

    @Test
    public void testOverrideAgility() throws Exception {
        Method agility = queenWitchClass.getDeclaredMethod("agility");
        assertEquals("java.lang.Integer", agility.getGenericReturnType().getTypeName());
        assertEquals(0, agility.getParameterCount());
    }
}
