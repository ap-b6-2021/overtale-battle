package id.ui.cs.overtale_battle.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import id.ui.cs.overtale_battle.core.entities.Monster;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class MonsterJsonSerializer extends JsonSerializer<Monster> {

    @Override
    public void serialize(Monster monster, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("type", monster.getMonsterAttacks().getClass().getSimpleName());
        gen.writeStringField("health", String.valueOf(monster.getHealth()));
        gen.writeStringField("state", monster.getState().toString());
        gen.writeStringField("maxHealth", String.valueOf(monster.getMaxHealth()));
        gen.writeStringField("id", monster.getId());
        gen.writeStringField("name", monster.getName());
        gen.writeEndObject();
    }
}
