package id.ui.cs.overtale_battle.repository;

import id.ui.cs.overtale_battle.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {
     Player findByName(String name);
}
