package id.ui.cs.overtale_battle.repository;

import id.ui.cs.overtale_battle.core.battle.Party;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Repository
public class PartyRepository {
    private final Map<String, Party> parties = new ConcurrentHashMap<>();

    public List<Party> findAll() {
        return new ArrayList<>(parties.values());
    }

    public Party findByPartyName(String name) {
        return parties.get(name);
    }

    public void save(Party party) {
        parties.put(party.getPartyName(), party);
    }

    public void delete(Party party) {
        parties.remove(party.getPartyName());
    }
}
