package id.ui.cs.overtale_battle.repository;

import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import id.ui.cs.overtale_battle.exceptions.PlayerInBattleException;
import id.ui.cs.overtale_battle.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.parser.Entity;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class BattlePlayerRepository {
    @Autowired
    private PlayerRepository playerRepository;

    private final Map<String, BattlePlayer> battlePlayers = new ConcurrentHashMap<>();

    public BattlePlayer findByName(String name) {
        return battlePlayers.get(name);
    }

    public BattlePlayer save(Player player) throws PlayerInBattleException {
        var player1 = battlePlayers.get(player.getName());
        if (player1 != null &&
            player1.getParty() != null &&
            player1.getParty().getStatus().equals(PartyState.STARTED)) {
            throw new PlayerInBattleException("Player is in battle!");
        }
        playerRepository.save(player);
        if (player1 == null) {
            player1 = new BattlePlayer(player);
            save(player1);
        }
        player1.update(player);
        battlePlayers.put(player.getName(), player1);
        return player1;
    }

    public BattlePlayer save(BattlePlayer player) {
        battlePlayers.put(player.getPlayer().getName(), player);
        return player;
    }
}
