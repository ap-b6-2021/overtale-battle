package id.ui.cs.overtale_battle.core.enums;

public enum PartyState {
    OPEN, STARTED, FINISHED
}
