package id.ui.cs.overtale_battle.core.entities.monsters;

import id.ui.cs.overtale_battle.core.entities.MonsterAttacks;

public class Dragon extends MonsterAttacks {

    private static final String name = "Jungle Dragon";

    @Override
    protected Integer attackDamage() {
        return 8;
    }
    @Override
    protected Integer defense() {
        return 5;
    }

    @Override
    protected Integer agility() {
        return 10;
    }

    @Override
    protected String getName() {
        return name;
    }

}
