package id.ui.cs.overtale_battle.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import lombok.Data;

@Data
public class Monster extends Entity {

    @JsonIgnore
    MonsterAttacks monsterAttacks;

    String id;

    public Monster(MonsterAttacks monsterAttacks, String id) {
        this.monsterAttacks = monsterAttacks;
        this.health = monsterAttacks.health();
        this.state = EntityState.ALIVE;
        this.id = id;
    }

    @Override
    public int getDefense() {
        return monsterAttacks.defense() + super.getDefense();
    }

    @Override
    public int getAttack() {
        return monsterAttacks.attackDamage() + super.getAttack();
    }

    public int getMaxHealth() { return monsterAttacks.health(); };

    public String getName() {
        return monsterAttacks.getName();
    }
}
