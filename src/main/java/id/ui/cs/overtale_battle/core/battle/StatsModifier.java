package id.ui.cs.overtale_battle.core.battle;

import lombok.Data;

@Data
public class StatsModifier {
    private int maxHealth;

    private int health;

    private int attackDamage;

    private int defense;

    private int agility;

    private int maxMana;

    private int mana;

    private int healthRegen;

    private int manaRegen;
}
