package id.ui.cs.overtale_battle.core.enums;

public enum AttackType {
    ATTACK, CONSUME, RUN
}
