package id.ui.cs.overtale_battle.core.entities;

import id.ui.cs.overtale_battle.model.DTOs.DamageDto;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Entity {
    EntityState state;

    List<StatsModifier> statsModifiers = new ArrayList<>();

    int health;

    public void processDamage(DamageDto damageDto) {
        this.health -= damageDto.getDamage();
    }

    public void processStatsModifier(StatsModifier statsModifier) {
        this.statsModifiers.add(statsModifier);
    }

    public int getAttack() {
        var atk = 0;
        for (StatsModifier mod : statsModifiers) {
            atk += mod.getAttackDamage();
        }
        return atk;
    }

    public int getDefense() {
        var def = 0;
        for (StatsModifier mod : statsModifiers) {
            def += mod.getDefense();
        }
        return def;
    }

    public void refreshState() {
        this.health = Math.max(health, 0);
        if (health <= 0) state = EntityState.DEAD;
    }
}
