package id.ui.cs.overtale_battle.core.battle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.entities.Monster;
import id.ui.cs.overtale_battle.core.enums.BattleResult;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class Party {

    private String partyName;

    private Date startTime;

    private Date endTime;

    private PartyState status;

    private List<BattlePlayer> players = new ArrayList<>();

    @JsonIgnore
    private Integer turnI;

    private String turn;

    private BattlePlayer partyLeader;

    private Map<Integer, Monster> monsters = new HashMap<>();

    private BattleResult result;

    public Party(String partyName, BattlePlayer leader) {
        this.partyName = partyName;
        this.partyLeader = leader;
        this.status = PartyState.OPEN;
        players.add(leader);
        leader.setParty(this);
    }

    public void refreshTurn() {
        if (players.isEmpty() || !status.equals(PartyState.STARTED) || getPlayers(EntityState.ALIVE).isEmpty()) return;
        if (turn == null) {
            this.turnI = 0;
        }
        do {
            this.turnI = (turnI + 1) % players.size();
        } while (!players.get(this.turnI).getState().equals(EntityState.ALIVE));

        this.turn = players.get(this.turnI).getPlayer().getName();
    }

    public void refreshState() {
        if (!status.equals(PartyState.STARTED)) return;

        for (BattlePlayer player : players) {
            player.refreshState();
        }
        for (Monster monster : monsters.values()) {
            monster.refreshState();
        }
        List<BattlePlayer> alivePlayers = getPlayers(EntityState.ALIVE);
        List<Monster> aliveMonster = getMonsters(EntityState.ALIVE);
        if (!alivePlayers.isEmpty() && aliveMonster.isEmpty()) result = BattleResult.VICTORY;
        else if (alivePlayers.isEmpty() && !aliveMonster.isEmpty()) result = BattleResult.DEFEAT;
        else if (alivePlayers.isEmpty() && aliveMonster.isEmpty()) result = BattleResult.DRAW;
        else result = null;

        if (result != null) status = PartyState.FINISHED;
    }

    public List<BattlePlayer> getPlayers(EntityState state) {
        return players.stream()
                      .filter(player -> player.getState().equals(state))
                      .collect(Collectors.toList());
    }

    public List<Monster> getMonsters(EntityState state) {
        return monsters.values()
                       .stream()
                       .filter(monster -> monster.getState().equals(state))
                       .collect(Collectors.toList());
    }
}
