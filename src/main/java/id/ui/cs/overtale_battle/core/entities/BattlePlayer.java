package id.ui.cs.overtale_battle.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.battle.Reward;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import id.ui.cs.overtale_battle.model.Player;
import lombok.Data;

@Data
public class BattlePlayer extends Entity {
    private Player player;

    @JsonIgnore
    private Party party;

    private int mana;

    private Reward reward;

    public BattlePlayer(Player player) {
        this.player = player;
        this.health = player.getMaxHealth();
        this.mana = player.getMaxMana();
        this.state = EntityState.ALIVE;
    }

    @Override
    public int getAttack() {
        return player.getAttackDamage() + super.getAttack();
    }

    @Override
    public int getDefense() {
        return player.getDefense() + super.getDefense();
    }

    public void update(Player player) {
        this.player = player;
        this.health = player.getMaxHealth();
        this.mana = player.getMaxMana();
        this.state = EntityState.ALIVE;
    }
}
