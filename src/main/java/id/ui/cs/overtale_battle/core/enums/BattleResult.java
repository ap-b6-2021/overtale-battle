package id.ui.cs.overtale_battle.core.enums;

public enum BattleResult {
    VICTORY, DEFEAT, DRAW
}
