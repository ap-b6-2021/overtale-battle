package id.ui.cs.overtale_battle.core.battle;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Reward {
    private int exp;
}
