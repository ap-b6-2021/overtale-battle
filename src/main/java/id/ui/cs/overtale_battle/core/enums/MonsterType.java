package id.ui.cs.overtale_battle.core.enums;

public enum MonsterType {
    KING_OGRE, DRAGON, QUEEN_WITCH
}
