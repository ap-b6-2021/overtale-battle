package id.ui.cs.overtale_battle.core.entities.monsters;

import id.ui.cs.overtale_battle.core.entities.MonsterAttacks;

public class KingOgre extends MonsterAttacks {

    private static final String name = "King Ogre";

    @Override
    protected Integer attackDamage() {
        return 10;
    }
    @Override
    protected Integer defense() {
        return 8;
    }

    @Override
    protected Integer agility() {
        return 4;
    }

    @Override
    protected String getName() {
        return name;
    }
}
