package id.ui.cs.overtale_battle.controller;

import id.ui.cs.overtale_battle.core.battle.Reward;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.model.DTOs.AttackDto;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.service.BattleService;
import id.ui.cs.overtale_battle.service.PartyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/battle")
@Slf4j
public class BattleController {
    @Autowired
    PartyService partyService;

    @Autowired
    BattleService battleService;

    @GetMapping(path = "/get/{party}/monsters", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getMonsters(@PathVariable(value = "party") String party) {
        var party1 = partyService.getParty(party);
        if (party1 == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(party1.getMonsters());
    }

    @GetMapping(path = "/get/reward/{playerName}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getReward(@PathVariable(value = "playerName") String name) {
        BattlePlayer player = partyService.getPlayer(name);
        if (player == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Player not found!");
        Reward reward = player.getReward();
        if (reward == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Player has no reward!");
        player.setReward(null);
        return ResponseEntity.ok(reward);
    }

    @PostMapping(path = "/start/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> startBattle(@PathVariable(value = "name") String name,
                                              @RequestBody List<Player> players) {
        try {
            Party party = battleService.startBattle(name, players);
            return ResponseEntity.ok(party);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @PostMapping(path = "/attack/{player}/{monster_id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> attack(@PathVariable(value = "player") String player,
                                         @PathVariable(value = "monster_id") int monsterId) {
        try {
            Map<String, AttackDto> attackResponse = battleService.attack(player, monsterId);
            return ResponseEntity.ok(attackResponse);
        } catch (Exception e) {
            log.error("Error", e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @PostMapping(path = "/buff/{player}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> buff(@PathVariable(value = "player") String playerName,
                                       @RequestBody StatsModifier item) {
        try {
            return ResponseEntity.ok(battleService.buff(playerName, item));
        } catch (Exception e) {
            log.error("Error", e);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }
}
