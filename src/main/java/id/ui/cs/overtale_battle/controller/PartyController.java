package id.ui.cs.overtale_battle.controller;

import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.model.DTOs.PartyLstDto;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.service.PartyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/party")
@Slf4j
public class PartyController {

    @Autowired
    private PartyService partyService;

    private String getErrMsg(Exception e) {
        String msg = e.getMessage();
        return msg == null ? "Internal Server Error" : msg;
    }

    @GetMapping(path = "/get", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PartyLstDto>> getParties() {
        List<Party> parties = partyService.getParties();
        List<PartyLstDto> partyLstDtos = new ArrayList<>();
        for (Party party : parties) {
            partyLstDtos.add(new PartyLstDto(party));
        }
        return ResponseEntity.ok(partyLstDtos);
    }

    @GetMapping(path = "/get/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getParty(@PathVariable(value = "name") String name) {
        var party = partyService.getParty(name);
        if (party != null) return ResponseEntity.ok(party);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format("Party with name %s not found", name));
    }

    @GetMapping(path = "/get/player/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPartyFromPlayer(@PathVariable(value = "name") String playerName) {
        var player = partyService.getPlayer(playerName);
        if (player == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format("Player with name %s not found!", playerName));
        if (player.getParty() == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format("Player with name %s isn't in a party!", playerName));
        return ResponseEntity.ok(player.getParty());
    }

    @GetMapping(path = "/get/playerInfo/{playerName}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> getPlayer(@PathVariable(value = "playerName") String playerName) {
        var player = partyService.getPlayer(playerName);
        if (player == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format("Player with name %s not found!", playerName));
        return ResponseEntity.ok(player);
    }

    @PostMapping(path = "/create/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<?> createParty(@PathVariable(value = "name") String partyName,
                                         @RequestBody Player player) {
        try {
            var party = partyService.createParty(partyName, player);
            if (party != null) return ResponseEntity.ok(party);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(getErrMsg(e));
        }
    }

    @PutMapping(path = "/add/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Object> addPlayer(@PathVariable(value = "name") String partyName,
                                            @RequestBody Player player) {
        Party party = null;
        try {
            party = partyService.addPlayer(partyName, player);
            if (party != null) return ResponseEntity.ok(party);
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(getErrMsg(e));
        }
    }

    @DeleteMapping(path = "/disband/{name}")
    @ResponseBody
    public ResponseEntity<Object> disbandParty(@PathVariable(value = "name") String partyName) {
        try {
            if (partyService.disbandParty(partyName)) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(getErrMsg(e));
        }

    }

    @DeleteMapping(path = "/disband/player/{playerName}")
    @ResponseBody
    public ResponseEntity<Object> disbandPlayer(@PathVariable(value = "playerName") String playerName) {
        Party party = null;
        try {
            BattlePlayer player = partyService.getPlayer(playerName);
            party = player.getParty();
            if (party == null) throw new NullPointerException("No Party!");
        } catch (Exception e) {
            log.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        try {
            if (partyService.disbandPlayer(party.getPartyName(), playerName)) {
                return ResponseEntity.ok(party);
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(getErrMsg(e));
        }
    }

    @DeleteMapping(path = "/disband/{partyName}/{playerName}")
    @ResponseBody
    public ResponseEntity<Object> disbandPlayerFromParty(@PathVariable(value = "partyName") String partyName,
                                           @PathVariable(value = "playerName") String playerName) {
        try {
            if (partyService.disbandPlayer(partyName, playerName)) {
                return ResponseEntity.ok(partyService.getParty(partyName));
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(getErrMsg(e));
        }
    }
}
