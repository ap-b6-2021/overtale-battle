package id.ui.cs.overtale_battle.model;

import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "battle_player")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Player {
    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "max_health")
    private Integer maxHealth;

    @Column(name = "attack_damage")
    private Integer attackDamage;

    @Column(name = "defense")
    private Integer defense;

    @Column(name = "agility")
    private Integer agility;

    @Column(name = "max_mana")
    private Integer maxMana;

    @Column(name = "health_regen")
    private Integer healthRegen;

    @Column(name = "mana_regen")
    private Integer manaRegen;

    @Column(name = "player_class")
    private String playerClass;

    @Transient
    private StatsModifier armor;

    @Transient
    private Object weapon;

    public void appendStat(StatsModifier statsModifier) {
        this.attackDamage += statsModifier.getAttackDamage();
        this.defense += statsModifier.getDefense();
        this.agility += statsModifier.getAgility();
        this.maxHealth += statsModifier.getMaxHealth();
        this.maxHealth += statsModifier.getMaxHealth();
        this.maxMana += statsModifier.getMaxMana();
        this.healthRegen += statsModifier.getHealthRegen();
        this.manaRegen += statsModifier.getManaRegen();
    }
}
