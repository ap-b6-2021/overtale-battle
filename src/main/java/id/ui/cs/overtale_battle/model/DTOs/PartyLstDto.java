package id.ui.cs.overtale_battle.model.DTOs;

import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import lombok.Data;

@Data
public class PartyLstDto {
    private String name;
    private int playerNum;
    private boolean isOpen;
    public PartyLstDto(Party party) {
        this.name = party.getPartyName();
        this.playerNum = party.getPlayers().size();
        this.isOpen = party.getStatus().equals(PartyState.OPEN);
    }
}
