package id.ui.cs.overtale_battle.model.DTOs;

import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.core.entities.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AttackDto {
    Entity from;
    List<Entity> to;
    List<StatsModifier> modifier;
    List<DamageDto> attacks;
}
