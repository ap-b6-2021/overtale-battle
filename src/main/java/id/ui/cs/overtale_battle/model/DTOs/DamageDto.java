package id.ui.cs.overtale_battle.model.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DamageDto {
    int damage;
    boolean crit;
}
