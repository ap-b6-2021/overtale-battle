package id.ui.cs.overtale_battle.service;

import id.ui.cs.overtale_battle.core.battle.*;
import id.ui.cs.overtale_battle.core.entities.*;
import id.ui.cs.overtale_battle.core.entities.monsters.Dragon;
import id.ui.cs.overtale_battle.core.entities.monsters.KingOgre;
import id.ui.cs.overtale_battle.core.entities.monsters.QueenWitch;
import id.ui.cs.overtale_battle.core.enums.BattleResult;
import id.ui.cs.overtale_battle.core.enums.EntityState;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import id.ui.cs.overtale_battle.exceptions.*;
import id.ui.cs.overtale_battle.model.DTOs.AttackDto;
import id.ui.cs.overtale_battle.model.DTOs.DamageDto;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.repository.BattlePlayerRepository;
import id.ui.cs.overtale_battle.repository.PartyRepository;
import id.ui.cs.overtale_battle.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BattleServiceImpl implements BattleService {

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    BattlePlayerRepository battlePlayerRepository;

    Random random = new Random();

    @Override
    public Party startBattle(String partyName, List<Player> playerList) throws PartyNotFoundException, PlayerInBattleException, PartyNotOpenException {
        var party = partyRepository.findByPartyName(partyName);
        if (party == null) {
            throw new PartyNotFoundException(String.format("Party with name %s not found!", partyName));
        }
        if (!party.getStatus().equals(PartyState.OPEN)) {
            throw new PartyNotOpenException(String.format("Party with name %s is already started or finished!", partyName));
        }
        for (Player player : playerList) {
            battlePlayerRepository.save(player);
        }
        assignMonsters(party);
        party.setStatus(PartyState.STARTED);
        party.setStartTime(new Date());
        party.refreshTurn();
        return party;
    }

    private void assignMonsters(Party party) {
        Map<Integer, Monster> monsters = party.getMonsters();
        for (var i = 0; i < random.nextInt(2) + 1; i++) {
            MonsterAttacks monster;
            switch (random.nextInt(3)) {
                case 0:
                    monster = new Dragon();
                    break;
                case 1:
                    monster = new KingOgre();
                    break;
                default:
                    monster = new QueenWitch();
            }
            monsters.put(i, new Monster(monster, String.valueOf(i)));
        }
    }

    @Override
    public Map<String, AttackDto> buff(String playerName, StatsModifier modifier) throws PlayerNotFoundException, PlayerNotInPartyException, PartyNotStartedException, PlayerIsNotInTurnException, PlayerInBattleException {
        var player1 = battlePlayerRepository.findByName(playerName);
        if (player1 == null) throw new PlayerNotFoundException(String.format("Player with name %s not found!", playerName));
        var party = player1.getParty();
        if (party == null) throw new PlayerNotInPartyException(String.format("Player with name %s not in a party!", playerName));
        if (!party.getStatus().equals(PartyState.STARTED)) throw new PartyNotStartedException(String.format("Party with name %s is not yet started!", party.getPartyName()));
        if (!party.getTurn().equals(playerName)) throw new PlayerIsNotInTurnException(String.format("Player with name %s is not in their turn!", playerName));

        var player = player1.getPlayer();
        player.appendStat(modifier);
        player1.setHealth(player1.getHealth() + modifier.getHealth());
        player1.setMana(player1.getMana() + modifier.getMana());

        battlePlayerRepository.save(player1);
        party.refreshState();

        var alivePlayers = party.getPlayers(EntityState.ALIVE);
        var aliveMonsters = party.getMonsters(EntityState.ALIVE);

        Map<String, AttackDto> attackMaps = new HashMap<>();
        if (!aliveMonsters.isEmpty()) {
            var monster = aliveMonsters.get(random.nextInt(aliveMonsters.size()));
            var playerTarget = alivePlayers.get(random.nextInt(alivePlayers.size()));
            attackMaps.put("monster", monsterAttack(playerTarget, monster));
        }

        party.refreshState();
        party.refreshTurn();

        if (party.getStatus().equals(PartyState.FINISHED)) {
            assignReward(party);
        }

        return attackMaps;
    }

    @Override
    public Map<String, AttackDto> attack(String playerName, int monsterId) throws PlayerNotFoundException, PlayerNotInPartyException, PartyNotStartedException, MonsterDoesNotExistsException, PlayerNotAliveException, MonsterNotAliveException, PlayerIsNotInTurnException {
        var player = battlePlayerRepository.findByName(playerName);
        if (player == null) throw new PlayerNotFoundException(String.format("Player with name %s not found!", playerName));
        var party = player.getParty();
        if (party == null) throw new PlayerNotInPartyException(String.format("Player with name %s not in a party!", playerName));
        if (!party.getStatus().equals(PartyState.STARTED)) throw new PartyNotStartedException(String.format("Party with name %s is not yet started!", party.getPartyName()));
        if (!party.getMonsters().containsKey(monsterId)) throw new MonsterDoesNotExistsException(String.format("Monster with id %s does not exists!", monsterId));
        if (!party.getTurn().equals(playerName)) throw new PlayerIsNotInTurnException(String.format("Player with name %s is not in their turn!", playerName));

        //Player role
        var alivePlayers = party.getPlayers(EntityState.ALIVE);
        var aliveMonsters = party.getMonsters(EntityState.ALIVE);
        var monster = party.getMonsters().get(monsterId);
        if (!alivePlayers.contains(player)) throw new  PlayerNotAliveException(String.format("Player with name %s is not in alive state!", playerName));
        if (!aliveMonsters.contains(monster)) throw new MonsterNotAliveException(String.format("Monster %s is not in alive state!", monster.getClass().getSimpleName()));
        Map<String, AttackDto> attackMaps = new HashMap<>();
        attackMaps.put("player", playerAttack(player, monster));
        party.refreshState();
        partyRepository.save(party);

        //Monster role
        alivePlayers = party.getPlayers(EntityState.ALIVE);
        aliveMonsters = party.getMonsters(EntityState.ALIVE);

        if (!aliveMonsters.isEmpty()) {
            monster = aliveMonsters.get(random.nextInt(aliveMonsters.size()));
            var playerTarget = alivePlayers.get(random.nextInt(alivePlayers.size()));
            attackMaps.put("monster", monsterAttack(playerTarget, monster));
        }

        party.refreshState();
        party.refreshTurn();

        if (party.getStatus().equals(PartyState.FINISHED)) {
            assignReward(party);
        }

        return attackMaps;
    }

    private void assignReward(Party party) {
        Reward reward = new Reward(party.getResult().equals(BattleResult.VICTORY) ? 300 : 50);
        for (BattlePlayer player : party.getPlayers()) {
            player.setReward(reward);
        }
    }

    private AttackDto playerAttack(BattlePlayer battlePlayer, Monster monster) {
        int atk = battlePlayer.getAttack();
        int def = monster.getDefense();

        var damageDto = getDamageFromAtk(atk, def);

        monster.processDamage(damageDto);

        List<Entity> targets = new ArrayList<>();
        List<StatsModifier> statsModifiers = new ArrayList<>();
        List<DamageDto> damageDtos = new ArrayList<>();

        targets.add(monster);
        damageDtos.add(damageDto);

        return new AttackDto(battlePlayer, targets, statsModifiers, damageDtos);
    }

    private AttackDto monsterAttack(BattlePlayer player, Monster monster) {
        int atk = monster.getAttack();
        int def = player.getDefense();

        var damageDto = getDamageFromAtk(atk, def);

        player.processDamage(damageDto);

        List<Entity> targets = new ArrayList<>();
        List<StatsModifier> statsModifiers = new ArrayList<>();
        List<DamageDto> damageDtos = new ArrayList<>();

        targets.add(player);
        damageDtos.add(damageDto);

        return new AttackDto(monster, targets, statsModifiers, damageDtos);
    }

    private DamageDto getDamageFromAtk(int atkAttacker, int defDefender) {
        DamageDto damageDto = new DamageDto();

        //Base damage
        int damage = (atkAttacker * atkAttacker) / (atkAttacker + defDefender);

        //Rng
        if (random.nextInt(100) < 10) { //Crit
            damageDto.setCrit(true);
            damage = damage*2 + random.nextInt(((int) Math.ceil(damage * 0.05)) + 1); // 2x dmg + rng(5% dmg)
        } else {
            damageDto.setCrit(false);
            damage = damage + (random.nextInt(damage/2 + 1)); // dmg + rng(half dmg)
        }

        damageDto.setDamage(damageDto.getDamage() + damage);
        return damageDto;
    }
}
