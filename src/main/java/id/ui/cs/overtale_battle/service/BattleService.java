package id.ui.cs.overtale_battle.service;

import id.ui.cs.overtale_battle.model.DTOs.AttackDto;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.core.battle.StatsModifier;
import id.ui.cs.overtale_battle.exceptions.*;
import id.ui.cs.overtale_battle.model.Player;

import java.util.List;
import java.util.Map;

public interface BattleService {
    Party startBattle(String partyName, List<Player> playerList) throws PartyNotFoundException, PlayerInBattleException, PartyNotOpenException;
    Map<String, AttackDto> attack(String player, int monsterId) throws PlayerNotFoundException, PlayerNotInPartyException, PartyNotStartedException, MonsterDoesNotExistsException, PlayerNotAliveException, MonsterNotAliveException, PlayerIsNotInTurnException;
    Map<String, AttackDto> buff(String playerName, StatsModifier modifier) throws PlayerNotFoundException, PlayerNotInPartyException, PartyNotStartedException, PlayerIsNotInTurnException, PlayerInBattleException;
}
