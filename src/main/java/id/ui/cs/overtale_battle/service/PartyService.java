package id.ui.cs.overtale_battle.service;

import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.exceptions.*;

import java.util.List;

public interface PartyService {
    List<Party> getParties();
    Party getParty(String name);
    Party createParty(String partyName, Player leader) throws PlayerAlreadyInPartyException, PartyNameAlreadyTakenException, PlayerInBattleException;
    Party addPlayer(String partyName, Player player) throws PartyNotFoundException, PartyAlreadyStartedException, PartyAlreadyFullException, PlayerAlreadyInPartyException, PlayerInBattleException;
    boolean disbandParty(String partyName) throws PartyNotFoundException;
    boolean disbandPlayer(String partyName, String playerName) throws PlayerNotFoundException, PlayerNotInPartyException, PartyNotFoundException, PlayerIsPartyLeaderException;
    BattlePlayer getPlayer(String playerName);
}
