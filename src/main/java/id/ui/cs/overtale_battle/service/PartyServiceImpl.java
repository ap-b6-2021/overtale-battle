package id.ui.cs.overtale_battle.service;

import id.ui.cs.overtale_battle.core.entities.BattlePlayer;
import id.ui.cs.overtale_battle.core.enums.PartyState;
import id.ui.cs.overtale_battle.model.Player;
import id.ui.cs.overtale_battle.core.battle.Party;
import id.ui.cs.overtale_battle.repository.BattlePlayerRepository;
import id.ui.cs.overtale_battle.repository.PartyRepository;
import id.ui.cs.overtale_battle.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartyServiceImpl implements PartyService {
    @Autowired
    PartyRepository partyRepository;

    @Autowired
    BattlePlayerRepository playerRepository;

    @Override
    public List<Party> getParties() {
        return partyRepository.findAll();
    }

    @Override
    public Party getParty(String name) {
        return partyRepository.findByPartyName(name);
    }

    @Override
    public Party createParty(String partyName, Player leader) throws PlayerAlreadyInPartyException, PartyNameAlreadyTakenException, PlayerInBattleException {
        var battlePlayer = playerRepository.save(leader);

        if (battlePlayer.getParty() != null) {
            throw new PlayerAlreadyInPartyException(String.format("Player %s already in a party!", leader.getName()));
        };
        if (partyRepository.findByPartyName(partyName) != null) {
            throw new PartyNameAlreadyTakenException(String.format("Party with name %s already taken", partyName));
        }
        var party = new Party(partyName, battlePlayer);
        partyRepository.save(party);
        playerRepository.save(battlePlayer);
        return party;
    }

    @Override
    public Party addPlayer(String partyName, Player player) throws PartyNotFoundException, PartyAlreadyStartedException, PartyAlreadyFullException, PlayerAlreadyInPartyException, PlayerInBattleException {
        var battlePlayer = playerRepository.save(player);

        if (battlePlayer.getParty() != null) {
            throw new PlayerAlreadyInPartyException(String.format("Player %s already in a party!", player.getName()));
        }

        var party = getParty(partyName);
        if (party == null) {
            throw new PartyNotFoundException(String.format("Party with name %s not found!", partyName));
        }
        if (party.getStatus() != PartyState.OPEN) {
            throw new PartyAlreadyStartedException(String.format("Party with name %s is not open for joining!", partyName));
        }
        if (party.getPlayers().size() >= 4) {
            throw new PartyAlreadyFullException(String.format("Party with name %s already full!", partyName));
        }

        battlePlayer.setParty(party);
        party.getPlayers().add(battlePlayer);
        playerRepository.save(battlePlayer);
        partyRepository.save(party);
        return party;
    }

    @Override
    public boolean disbandParty(String partyName) throws PartyNotFoundException {
        var party = getParty(partyName);
        if (party == null) {
            throw new PartyNotFoundException(String.format("Party with name %s not found!", partyName));
        }

        for (BattlePlayer player : party.getPlayers()) {
            player.setParty(null);
            playerRepository.save(player);
        }

        partyRepository.delete(party);
        return true;
    }

    @Override
    public boolean disbandPlayer(String partyName, String playerName) throws PlayerNotFoundException, PlayerNotInPartyException, PartyNotFoundException, PlayerIsPartyLeaderException {
        var battlePlayer = playerRepository.findByName(playerName);

        if (battlePlayer == null) {
            throw new PlayerNotFoundException(String.format("Player with name %s not found!", playerName));
        }
        if (battlePlayer.getParty() == null) {
            throw new PlayerNotInPartyException(String.format("Player with name %s is not in party!", playerName));
        }
        if (battlePlayer.getParty().getPartyLeader().equals(battlePlayer)) {
            throw new PlayerIsPartyLeaderException(String.format("Player with name %s is the party leader! You cannot leave.", playerName));
        }

        Party party = battlePlayer.getParty();
        battlePlayer.getParty().getPlayers().remove(battlePlayer);
        battlePlayer.setParty(null);
        playerRepository.save(battlePlayer);
        partyRepository.save(party);
        return true;
    }

    @Override
    public BattlePlayer getPlayer(String playerName) {
        return playerRepository.findByName(playerName);
    }
}
