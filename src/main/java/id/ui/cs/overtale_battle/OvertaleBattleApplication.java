package id.ui.cs.overtale_battle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvertaleBattleApplication {
    public static void main(String[] args) {
        SpringApplication.run(OvertaleBattleApplication.class, args);
    }
}
