package id.ui.cs.overtale_battle.exceptions;

public class PlayerIsNotInTurnException extends Exception {
    public PlayerIsNotInTurnException(String msg) {
        super(msg);
    }
}
