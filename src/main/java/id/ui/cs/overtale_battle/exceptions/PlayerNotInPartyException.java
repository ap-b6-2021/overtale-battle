package id.ui.cs.overtale_battle.exceptions;

public class PlayerNotInPartyException extends Exception {
    public PlayerNotInPartyException(String msg) {
        super(msg);
    }
}
