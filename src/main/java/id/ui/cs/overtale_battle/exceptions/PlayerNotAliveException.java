package id.ui.cs.overtale_battle.exceptions;

public class PlayerNotAliveException extends Exception {
    public PlayerNotAliveException(String msg) {
        super(msg);
    }
}
