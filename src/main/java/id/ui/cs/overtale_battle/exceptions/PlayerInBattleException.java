package id.ui.cs.overtale_battle.exceptions;

public class PlayerInBattleException extends Exception {
    public PlayerInBattleException(String msg) {
        super(msg);
    }
}
