package id.ui.cs.overtale_battle.exceptions;

public class PlayerNotFoundException extends Exception {
    public PlayerNotFoundException(String msg) {
        super(msg);
    }
}
