package id.ui.cs.overtale_battle.exceptions;

public class PlayerIsPartyLeaderException extends Exception {
    public PlayerIsPartyLeaderException(String msg) {
        super(msg);
    }
}
