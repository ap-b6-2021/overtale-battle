package id.ui.cs.overtale_battle.exceptions;

public class MonsterDoesNotExistsException extends Exception {
    public MonsterDoesNotExistsException(String msg) {
        super(msg);
    }
}
