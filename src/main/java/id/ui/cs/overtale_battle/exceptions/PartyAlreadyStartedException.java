package id.ui.cs.overtale_battle.exceptions;

public class PartyAlreadyStartedException extends Exception {
    public PartyAlreadyStartedException(String msg) {
        super(msg);
    }
}
