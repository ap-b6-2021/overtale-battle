package id.ui.cs.overtale_battle.exceptions;

public class MonsterNotAliveException extends Exception {
    public MonsterNotAliveException(String msg) {
        super(msg);
    }
}
