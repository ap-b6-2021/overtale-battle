package id.ui.cs.overtale_battle.exceptions;

public class PartyNameAlreadyTakenException extends Exception {
    public PartyNameAlreadyTakenException(String msg) {
        super(msg);
    }
}
