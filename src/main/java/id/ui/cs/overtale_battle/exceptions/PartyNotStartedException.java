package id.ui.cs.overtale_battle.exceptions;

public class PartyNotStartedException extends Exception {
    public PartyNotStartedException(String msg) {
        super(msg);
    }
}
