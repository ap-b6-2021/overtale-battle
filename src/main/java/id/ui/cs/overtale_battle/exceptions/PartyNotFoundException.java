package id.ui.cs.overtale_battle.exceptions;

public class PartyNotFoundException extends Exception {
    public PartyNotFoundException(String msg) {
        super(msg);
    }
}
