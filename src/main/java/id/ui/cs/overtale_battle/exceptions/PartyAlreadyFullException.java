package id.ui.cs.overtale_battle.exceptions;

public class PartyAlreadyFullException extends Exception {
    public PartyAlreadyFullException(String msg) {
        super(msg);
    }
}
