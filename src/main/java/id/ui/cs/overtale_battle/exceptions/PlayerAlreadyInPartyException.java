package id.ui.cs.overtale_battle.exceptions;

public class PlayerAlreadyInPartyException extends Exception{
    public PlayerAlreadyInPartyException(String msg) {
        super(msg);
    }
}
