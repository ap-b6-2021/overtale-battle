package id.ui.cs.overtale_battle.exceptions;

public class PartyNotOpenException extends Exception {
    public PartyNotOpenException(String msg) {
        super(msg);
    }
}
