[![pipeline status](https://gitlab.com/ap-b6-2021/overtale-battle/badges/master/pipeline.svg)](https://gitlab.com/ap-b6-2021/overtale-battle/-/commits/master)
[![coverage report](https://gitlab.com/ap-b6-2021/overtale-battle/badges/master/coverage.svg)](https://gitlab.com/ap-b6-2021/overtale-battle/-/commits/master)

Overtle Battle Microservice

# Deskripsi
Overtale microservice for PvE players with monster.

# Link
- Main Service: https://gitlab.com/ap-b6-2021/overtale
- Battle Service: https://gitlab.com/ap-b6-2021/overtale-battle